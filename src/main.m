/* main.m */
#import <Foundation/Foundation.h>
#include "objcpoint.h"

/* Home-made macro for console printing. */
#define PUTERR(FORMAT, ...) \
    fprintf(stderr, "%s\n", \
        [[NSString stringWithFormat:FORMAT, \
            ##__VA_ARGS__] \
                UTF8String]);


int main(void)
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    if (!pool)
        return 1;

    ObjCPoint *p = nil;
    ObjCPoint *q = nil;
    double d;

    p = [[[ObjCPoint alloc] init] autorelease];
    if (!p) {
        PUTERR(@"Fails to create p");
        goto ERROR_MAIN;
    }

    q = [[[ObjCPoint alloc] initWithX:3.0 Y:4.0] autorelease];
    if (!q) {
        PUTERR(@"Fails to create q");
        goto ERROR_MAIN;
    }

    if (!(3.0 == [q x])) {
        PUTERR(@"Wrong x value: %.2f", [q x]);
        goto ERROR_MAIN;
    }

    if (!(4.0 == [q y])) {
        PUTERR(@"Wrong y value: %.2f", [q y]);
        goto ERROR_MAIN;
    }

    d = [ObjCPoint distanceBetween:p andPoint:q];
    if (!(5.0 == d)) {
        PUTERR(@"Wrong distance: %.2f", d);
        goto ERROR_MAIN;
    }

    [q setX:5.0];
    [q setY:12.0];
    
    d = [ObjCPoint distanceBetween:p andPoint:q];
    if (!(13.0 == d)) {
        PUTERR(@"Wrong distance: %.2f", d);
        goto ERROR_MAIN;
    }

    [pool drain];

    return 0;

ERROR_MAIN:
    [pool drain];

    return 1;
}
