/* objcpoint.m */
#import <Foundation/Foundation.h>
#include <stdlib.h>
#include "point.h"
#import "objcpoint.h"

struct objcpoint_private_t {
    point_t *pt;
};


@interface ObjCPoint ()
-(objcpoint_private_t *) fields;
@end

@implementation ObjCPoint
+(double) distanceBetween:(ObjCPoint *)p andPoint:(ObjCPoint *)q
{
    return point_distance_between([p fields]->pt, [q fields]->pt);
}

-(instancetype) init
{
    return [self initWithX:0.0 Y:0.0];
}

-(instancetype) initWithX:(double)x Y:(double)y
{
    if (!self)
        return self;

    fields = \
        (objcpoint_private_t *) malloc(sizeof(objcpoint_private_t));
    if (!fields) {
        [self release];
        self = nil;
        return self;
    }

    fields->pt = point_new(x, y);
    if (!(fields->pt)) {
        free(fields);
        [self release];
        self = nil;
        return self;
    }

    return self;
}

-(void) dealloc
{
    if (!self)
        return;

    point_delete((void *) (fields->pt));

    if (fields)
        free(fields);

    [super dealloc];
}

-(objcpoint_private_t *) fields
{
    return fields;
}

-(double) x
{
    return point_x(fields->pt);
}

-(void) setX:(double)x
{
    point_set_x(fields->pt, x);
}

-(double) y
{
    return point_y(fields->pt);
}

-(void) setY:(double)y
{
    point_set_y(fields->pt, y);
}
@end
