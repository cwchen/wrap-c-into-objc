/* objcpoint.h */
#pragma once

#import <Foundation/Foundation.h>

typedef struct objcpoint_private_t objcpoint_private_t;

@interface ObjCPoint : NSObject {
    objcpoint_private_t *fields;
}

+(double) distanceBetween:(ObjCPoint *)p andPoint:(ObjCPoint *)q;
-(instancetype) init;
-(instancetype) initWithX:(double)x Y:(double)y;
-(void) dealloc;
-(double) x;
-(void) setX:(double)x;
-(double) y;
-(void) setY:(double)y;
@end
